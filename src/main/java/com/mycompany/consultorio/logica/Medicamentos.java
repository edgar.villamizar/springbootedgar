/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.consultorio.logica;

//import java.sql.Connection;
import java.sql.SQLException;
//import java.sql.Statement;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Edgar
 */
@Component("medicamentos")
public class Medicamentos {
    // Configuracion Springboot
    @Autowired
    transient JdbcTemplate jdbcTemplate;
    //Atributos
    private String idMedicamento;
    private String nomMedicamento;
    private String presentacionMedicamento;
    
    //Constructor por defecto
    
    public Medicamentos(String idMedicamento, String nomMedicamento, String presentacionMedicamento) {
        this.idMedicamento = idMedicamento;
        this.nomMedicamento = nomMedicamento;
        this.presentacionMedicamento = presentacionMedicamento;
    }
    
    //Constructor con los atributos
    public Medicamentos(){
        
    }
    
    //Metodos
    
    //Getters (obtener) and setters (establecer)
   
    public String getIdMedicamento() {
        return idMedicamento;
    }

    public void setIdMedicamento(String idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public String getNomMedicamento() {
        return nomMedicamento;
    }

    public void setNomMedicamento(String nomMedicamento) {
        this.nomMedicamento = nomMedicamento;
    }

    public String getPresentacionMedicamento() {
        return presentacionMedicamento;
    }

    public void setPresentacionMedicamento(String presentacionMedicamento) {
        this.presentacionMedicamento = presentacionMedicamento;
    }
           
    //CRUD
    // Guardar
    public String guardar() throws SQLException, ClassNotFoundException{ // ClassNotFoundException
        // public void --> public String
        // clase de excepcion, de errores que puedan surgir
        // Sentencia que asigna los valores a la tabla medicamentos
        String sql = "INSERT INTO medicamentos (idMedicamento, nomMedicamento, presentacionMedicamento) VALUES (?,?,?);"; // consulta a la BD
        return sql;
    }
    // Consultar
    public boolean consultar() throws SQLException, ClassNotFoundException{
        // consulta la fila que contenga el mismo idMedicamento
        // Una funcion puede ser escrita de dos formas. La larga: public String funcion(Tipo1 param1, Tipo2 param2){
        //  return Booleano;
        // }
        // Una forma "inline" osea corta: (param1, param2) -> unString;  
        String sql = "SELECT idMedicamento, nomMedicamento, presentacionMedicamento FROM medicamentos WHERE idMedicamento = ?;";
        // crear una lista para recorrer
        // <nombre de esta clase> nombre tabla de la BD
         List<Medicamentos> medicamentos = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Medicamentos(
                        rs.getString("idMedicamento"), // => cursor o apuntador a la columna Login de la tabla Usuarios
                        rs.getString("nomMedicamento"),
                        rs.getString("presentacionMedicamento")
                ), new Object[]{this.getIdMedicamento()});
        if (medicamentos != null && medicamentos.size() > 0) { // se evalua si existe usuario con ese login
            this.setIdMedicamento(medicamentos.get(0).getIdMedicamento());
            this.setNomMedicamento(medicamentos.get(0).getNomMedicamento());
            this.setPresentacionMedicamento(medicamentos.get(0).getPresentacionMedicamento());
            return true;
        } else {
            return false;
        }       
    }
    // Actualizar
    public String actualizar () throws SQLException, ClassNotFoundException{
        String sql = "UPDATE medicamentos SET idMedicamento = ?, nomMedicamento = ?,presentacionMedicamento = ? WHERE idMedicamentos = ?;";
        return sql;
    }
    // Borrar
    public String borrar() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM medicamentos WHERE idMedicamento = ?;";
        return sql;
    }
    
    //toString
    
    @Override
    public String toString() {
        return "Medicamentos{" + "idMedicamento=" + idMedicamento + ", nomMedicamento=" + nomMedicamento + ", presentacionMedicamento=" + presentacionMedicamento + '}';
    }    
}
