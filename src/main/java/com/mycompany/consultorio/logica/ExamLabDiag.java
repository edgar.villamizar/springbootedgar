/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.consultorio.logica;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Edgar
 */
@Component("examLabDiag")
// nombre de la tabal en la base de datos= examLabDiag
public class ExamLabDiag {
    // Configuracion Springboot
    @Autowired
    
    transient JdbcTemplate jdbcTemplate;
    
    // Atributos
    private String idExam;
    private String nomExam;
    
    // Constructor

    public ExamLabDiag(String idExam, String nomExam) {
        this.idExam = idExam;
        this.nomExam = nomExam;
    }

    public ExamLabDiag() {
    }
    
    // Metodos
    // getters ans setters

    public String getIdExam() {
        return idExam;
    }

    public void setIdExam(String idExam) {
        this.idExam = idExam;
    }

    public String getNomExam() {
        return nomExam;
    }

    public void setNomExam(String nomExam) {
        this.nomExam = nomExam;
    }

    // CRUD
    // Guardar
    public String guardar() throws SQLException, ClassNotFoundException{
        // public void --> public String
        // Sentencia que asigna los valores a la tabla examLabDiag
        String sql = "INSERT INTO examLabDiag (idExam, nomExam) VALUES (?,?)";
        return sql;
    }
    // Consultar
    public boolean consultar() throws SQLException, ClassNotFoundException{
        // consulta la fila que contenga el mismo idExam
        // Una funcion puede ser escrita de dos formas. La larga: public String funcion(Tipo1 param1, Tipo2 param2){
        //  return un booleano;
        // }
        // Una forma "inline" osea corta: (param1, param2) -> unString;        
        String sql = "SELECT idExam, nomExam FROM examLabDiag WHERE idExam = ?;";
        // crear una lista para recorrer
        // <nombre de esta clase> nombre tabla de la BD
        List<ExamLabDiag> examenes = jdbcTemplate.query(sql, (rs, rowNum)
                -> new ExamLabDiag(
                        rs.getString("idExam"), // => cursor o apuntador a la columna idExam de la tabla examLabDiag
                        rs.getString("nomExam")
                ), new Object[]{this.getIdExam()});
        if (examenes != null && examenes.size() > 0) { // se evalua si existe examen con ese idExam
            this.setIdExam(examenes.get(0).getIdExam());
            this.setNomExam(examenes.get(0).getNomExam());
            return true;
        } else {
            return false;
        }
    }    
    // Actualizar
    public String actualizar () throws SQLException, ClassNotFoundException{
        String sql = "UPDATE examLabDiag SET idExam = ?, nomExam = ? WHERE idExam = ?;"; //#idExamn no se debe poder actualizar
        return sql;
    }
    // Borrar
    public String borrar() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM examLabDiag WHERE idExam =?;";
        return sql;
    }
    
    @Override
    public String toString() {
        return "ExamLabDiag{" + "idExam=" + idExam + ", nomExam=" + nomExam + '}';
    }
    
}
