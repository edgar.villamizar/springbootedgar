package com.mycompany.consultorio.logica;

//import java.sql.*;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
import java.sql.SQLException;
//import java.sql.Date;
//import java.sql.Statement;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Edgar
 */
@Component("usuarios") // en springboot los modelos pasan a ser componentes
// nombre de la tabla en la base de datos = usuarios
public class Usuario {
    // Configuracio springboot
    @Autowired // sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva
    // no hay que hacer objeto a = new objeto
    transient JdbcTemplate jdbcTemplate; // instancia el cual es el equivalente al statement de la conexion
    
    // Atributos
    private String Login;
    private String password;
    private String Nombre1;
    private String Nombre2;
    private String Apellido1;
    private String Apellido2;
    private String tipoId_idTipoId;
    private String identificacion;
    private String perfilUsuario_idPerfilUsuario;
    private String email;
    private String telefono;
    private String direccion;
    private String ciudad;
    private String pais;
    
    // Constructor 

    public Usuario(String Login, String password, String Nombre1, String Nombre2, String Apellido1, String Apellido2, String tipoId_idTipoId, String identificacion, String perfilUsuario_idPerfilUsuario, String email, String telefono, String direccion, String ciudad, String pais) {
        this.Login = Login;
        this.password = password;
        this.Nombre1 = Nombre1;
        this.Nombre2 = Nombre2;
        this.Apellido1 = Apellido1;
        this.Apellido2 = Apellido2;
        this.tipoId_idTipoId = tipoId_idTipoId;
        this.identificacion = identificacion;
        this.perfilUsuario_idPerfilUsuario = perfilUsuario_idPerfilUsuario;
        this.email = email;
        this.telefono = telefono;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.pais = pais;
    }
    
    public Usuario(){
        
    }

    // Metodos
    // getters ans setters

    public String getLogin() {
        return Login;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getNombre1() {
        return Nombre1;
    }

    public void setNombre1(String Nombre1) {
        this.Nombre1 = Nombre1;
    }

    public String getNombre2() {
        return Nombre2;
    }

    public void setNombre2(String Nombre2) {
        this.Nombre2 = Nombre2;
    }

    public String getApellido1() {
        return Apellido1;
    }

    public void setApellido1(String Apellido1) {
        this.Apellido1 = Apellido1;
    }

    public String getApellido2() {
        return Apellido2;
    }

    public void setApellido2(String Apellido2) {
        this.Apellido2 = Apellido2;
    }

    public String getTipoId_idTipoId() {
        return tipoId_idTipoId;
    }

    public void setTipoId_idTipoId(String tipoId_idTipoId) {
        this.tipoId_idTipoId = tipoId_idTipoId;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPerfilUsuario_idPerfilUsuario() {
        return perfilUsuario_idPerfilUsuario;
    }

    public void setPerfilUsuario_idPerfilUsuario(String perfilUsuario_idPerfilUsuario) {
        this.perfilUsuario_idPerfilUsuario = perfilUsuario_idPerfilUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
     
    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    // CRUD
    // Guardar
    public String guardar() throws SQLException, ClassNotFoundException{ // ClassNotFoundException
        // public void --> public String
        // clase de excepcion, de errores que puedan surgir
        // Sentencia que asigna los valores a la tabla usuarios
        String sql = "INSERT INTO usuarios (Login, password, Nombre1, Nombre2, Apellido1, Apellido2, tipoId_idTipoId, identificacion, "
                + "perfilUsuario_idPerfilUsuario, email, telefono, direccion, ciudad, pais) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        return sql;
    }
    // Consultar
    public boolean consultar() throws SQLException, ClassNotFoundException{
        // consulta la fila que contenga el mismo Login
        // Una funcion puede ser escrita de dos formas. La larga: public String funcion(Tipo1 param1, Tipo2 param2){
        //  return un String;
        // }
        // Una forma "inline" osea corta: (param1, param2) -> unString;        
        String sql = "SELECT Login, password, Nombre1, Nombre2, Apellido1, Apellido2, tipoId_idTipoId, identificacion, "
                + "perfilUsuario_idPerfilUsuario, email, telefono, direccion, ciudad, pais FROM usuarios WHERE Login = ?;";
        // crear una lista para recorrer
        // <nombre de esta clase> nombre tabla de la BD
        List<Usuario> usuarios = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Usuario(
                        rs.getString("Login"), // => cursor o apuntador a la columna Login de la tabla Usuarios
                        rs.getString("password"),
                        rs.getString("Nombre1"),
                        rs.getString("Nombre2"),
                        rs.getString("Apellido1"),
                        rs.getString("Apellido2"),
                        rs.getString("tipoId_idTipoId"), // cursor -> Foreign Key
                        rs.getString("identificacion"),
                        rs.getString("perfilUsuario_idPerfilUsuario"), // cursor -> Foreign Key
                        rs.getString("email"),
                        rs.getString("telefono"),
                        rs.getString("direccion"),
                        rs.getString("ciudad"),
                        rs.getString("pais")
                ), new Object[]{this.getLogin()});
        if (usuarios != null && usuarios.size() > 0) { // se evalua si existe usuario con ese login
            this.setLogin(usuarios.get(0).getLogin());
            this.setPassword(usuarios.get(0).getPassword());
            this.setNombre1(usuarios.get(0).getNombre1());
            this.setNombre2(usuarios.get(0).getNombre2());
            this.setApellido1(usuarios.get(0).getApellido1());
            this.setApellido2(usuarios.get(0).getApellido2());
            this.setTipoId_idTipoId(usuarios.get(0).getTipoId_idTipoId());
            this.setIdentificacion(usuarios.get(0).getIdentificacion());
            this.setPerfilUsuario_idPerfilUsuario(usuarios.get(0).getPerfilUsuario_idPerfilUsuario());
            this.setEmail(usuarios.get(0).getEmail());
            this.setTelefono(usuarios.get(0).getTelefono());
            this.setDireccion(usuarios.get(0).getDireccion());
            this.setCiudad(usuarios.get(0).getCiudad());
            this.setPais(usuarios.get(0).getPais());
            return true;
        } else {
            return false;
        }
    }
    // Actualizar
    public String actualizar () throws SQLException, ClassNotFoundException{
        String sql = "UPDATE usuarios SET Login = ?,password = ?,Nombre1 = ?,Nombre2 = ?,Apellido1 = ?,Apellido2 = ?,tipoId_idTipoId = ?,"
                + "identificacion = ?,perfilUsuario_idPerfilUsuario = ?,email = ?,telefono = ?,direccion = ?,ciudad = ?,pais = ? WHERE Login = ?;"; //#identificacion no se debe poder actualizar
        return sql;
    }
    // Borrar
    public String borrar() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM usuarios WHERE Login =?;";
        return sql;
    }

    // toString. para ver los valores de los atributos de la clase

    @Override
    public String toString() {
        return "Usuario{" + "Login=" + Login + " ,password=" + password + ", Nombre1=" + Nombre1 + ", Nombre2=" + Nombre2 + ", Apellido1=" + Apellido1 
                + ", Apellido2=" + Apellido2 + ", tipoId_idTipoId=" + tipoId_idTipoId + ", identificacion=" + identificacion 
                + ", perfilUsuario_idPerfilUsuario=" + perfilUsuario_idPerfilUsuario + ", email=" + email + ", telefono=" + telefono 
                + ", direccion=" + direccion + ", ciudad=" + ciudad + ", pais=" + pais + '}';
    }
    
}
