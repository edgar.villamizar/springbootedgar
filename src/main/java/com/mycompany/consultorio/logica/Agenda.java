/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.consultorio.logica;

import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Edgar
 */

@Component ("agenda")
// nombre de la tabla en la base de datos = agenda
public class Agenda {
    // Configuracion springboot
    @Autowired
    transient JdbcTemplate jdbcTemplate;
    
    // Atributos
    private String citaDia;
    private String citaHora;
    private String doctorUsuario;
    private String pacienteUsuario;
    private String diagnostico;
    private String medicamentos1;
    private String medicamentos2;
    private String medicamentos3;
    private String idExam1;
    private String idExam2;
    private String idExam3;
    
    // Constructor

    public Agenda(String citaDia, String citaHora, String doctorUsuario, String pacienteUsuario, String diagnostico, String medicamentos1, String medicamentos2, String medicamentos3, String idExam1, String idExam2, String idExam3) {
        this.citaDia = citaDia;
        this.citaHora = citaHora;
        this.doctorUsuario = doctorUsuario;
        this.pacienteUsuario = pacienteUsuario;
        this.diagnostico = diagnostico;
        this.medicamentos1 = medicamentos1;
        this.medicamentos2 = medicamentos2;
        this.medicamentos3 = medicamentos3;
        this.idExam1 = idExam1;
        this.idExam2 = idExam2;
        this.idExam3 = idExam3;
    }

    public Agenda() {
        //
    }
    
    // Metodos
    // getters ans setters

    public String getCitaDia() {
        return citaDia;
    }

    public void setCitaDia(String citaDia) {
        this.citaDia = citaDia;
    }

    public String getCitaHora() {
        return citaHora;
    }

    public void setCitaHora(String citaHora) {
        this.citaHora = citaHora;
    }

    public String getDoctorUsuario() {
        return doctorUsuario;
    }

    public void setDoctorUsuario(String doctorUsuario) {
        this.doctorUsuario = doctorUsuario;
    }

    public String getPacienteUsuario() {
        return pacienteUsuario;
    }

    public void setPacienteUsuario(String pacienteUsuario) {
        this.pacienteUsuario = pacienteUsuario;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getMedicamentos1() {
        return medicamentos1;
    }

    public void setMedicamentos1(String medicamentos1) {
        this.medicamentos1 = medicamentos1;
    }

    public String getMedicamentos2() {
        return medicamentos2;
    }

    public void setMedicamentos2(String medicamentos2) {
        this.medicamentos2 = medicamentos2;
    }

    public String getMedicamentos3() {
        return medicamentos3;
    }

    public void setMedicamentos3(String medicamentos3) {
        this.medicamentos3 = medicamentos3;
    }

    public String getIdExam1() {
        return idExam1;
    }

    public void setIdExam1(String idExam1) {
        this.idExam1 = idExam1;
    }

    public String getIdExam2() {
        return idExam2;
    }

    public void setIdExam2(String idExam2) {
        this.idExam2 = idExam2;
    }

    public String getIdExam3() {
        return idExam3;
    }

    public void setIdExam3(String idExam3) {
        this.idExam3 = idExam3;
    }

    // CRUD
    // Guardar
    public String guardar() throws SQLException, ClassNotFoundException{
        // Sentencia que asigna los valores a la tabla agenda
        String sql = "INSERT INTO agenda (citaDia, citaHora, doctorUsuario, pacienteUsuario, diagnostico, medicamentos1, medicamentos2, "
                + "medicamentos3, idExam1, idExam2, idExam3) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        return sql;
    }

    // Consultar
    public boolean consultar() throws SQLException, ClassNotFoundException{
        // consulta la fila que contenga el mismo citaDia
        // Una funcion puede ser escrita de dos formas. La larga: public String funcion(Tipo1 param1, Tipo2 param2){
        //  return un String;
        // }
        // Una forma "inline" osea corta: (param1, param2) -> unString;        
        String sql = "SELECT citaDia, citaHora, doctorUsuario, pacienteUsuario, diagnostico, medicamentos1, medicamentos2, "
                + "medicamentos3, idExam1, idExam2, idExam3 FROM agenda WHERE citaDia = ?;";
        // crear una lista para recorrer
        // <nombre de esta clase> nombre tabla de la BD
        List<Agenda> agendas = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Agenda(
                        rs.getString("citaDia"), // => cursor o apuntador a la columna citaDia de la tabla agenda
                        rs.getString("citaHora"),
                        rs.getString("doctorUsuario"), // cursor -> Foreign Key
                        rs.getString("pacienteUsuario"), // cursor -> Foreign Key
                        rs.getString("diagnostico"),
                        rs.getString("medicamentos1"), // cursor -> Foreign Key
                        rs.getString("medicamentos2"), // cursor -> Foreign Key
                        rs.getString("medicamentos3"), // cursor -> Foreign Key
                        rs.getString("idExam1"), // cursor -> Foreign Key
                        rs.getString("idExam2"),
                        rs.getString("idExam3")
                ), new Object[]{this.getCitaDia()});
        if (agendas != null && agendas.size() > 0) { // se evalua si existe usuario con esa citaDia
            this.setCitaDia(agendas.get(0).getCitaDia());
            this.setCitaHora(agendas.get(0).getCitaHora());
            this.setDoctorUsuario(agendas.get(0).getDoctorUsuario());
            this.setPacienteUsuario(agendas.get(0).getPacienteUsuario());
            this.setDiagnostico(agendas.get(0).getDiagnostico());
            this.setMedicamentos1(agendas.get(0).getMedicamentos1());
            this.setMedicamentos2(agendas.get(0).getMedicamentos2());
            this.setMedicamentos3(agendas.get(0).getMedicamentos3());
            this.setIdExam1(agendas.get(0).getIdExam1());
            this.setIdExam2(agendas.get(0).getIdExam2());
            this.setIdExam3(agendas.get(0).getIdExam3());
            return true;
        } else {
            return false;
        }
    }

    // Actualizar
    public String actualizar () throws SQLException, ClassNotFoundException{
        String sql = "UPDATE agenda SET citaDia = ?,citaHora = ?,doctorUsuario = ?,pacienteUsuario = ?,diagnostico = ?,medicamentos1 = ?,"
                + "medicamentos2 = ?,medicamentos3 = ?,idExam1 = ?,idExam2 = ?,idExam3 = ? WHERE citaDia = ?;"; // valor que no se debe poder actualizar
        return sql;
    }    

    // Borrar
    public String borrar() throws ClassNotFoundException, SQLException{
        String sql = "DELETE FROM agenda WHERE citaDia =?;";
        return sql;
    }
    
    // toString. para ver los valores de los atributos de la clase

    @Override
    public String toString() {
        return "Agenda{" + "citaDia=" + citaDia + ", citaHora=" + citaHora + ", doctorUsuario=" + doctorUsuario + ", pacienteUsuario=" + pacienteUsuario 
                + ", diagnostico=" + diagnostico + ", medicamentos1=" + medicamentos1 + ", medicamentos2=" + medicamentos2 + ", medicamentos3=" + medicamentos3 
                + ", idExam1=" + idExam1 + ", idExam2=" + idExam2 + ", idExam3=" + idExam3 + '}';
    }    
    
}
