package com.mycompany.consultorio;

import com.google.gson.Gson;
import com.mycompany.consultorio.logica.Usuario;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication // hace referencia a que es la clase Main
@RestController // clase tipo API Rest (transferencia de estado representacional), Rest se basa en una transferencia de recursos
public class DemoApplication {
    @Autowired // permite la conexion del jdbc template a la base de datos
    // JdbcTemplate jdbcTemplate; // instancia - jdbcTemplate que equivale al statement de la clase Conexion
    // Ventajas. // Ventajas: manejo de excepciones controlado.
    // Evita el SQL Injection que pueden ingresar los hackers porque valida las entradas de los parametros.
    //@Autowired // Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva. Equivalente a c = new Cuenta();
    Usuario e;
  
	public static void main(String[] args) {
            
		SpringApplication.run(DemoApplication.class, args);
                
	}
               
        @GetMapping("/usuario")
        public String consultarUsuario(@RequestParam(value = "Login", defaultValue = "admin") String Login) throws ClassNotFoundException, SQLException {
            e.setLogin(Login);
            if (e.consultar()) {
                String res = new Gson().toJson(e);
                e.setLogin("");
                e.setPassword("");
                e.setNombre1("");
                e.setNombre2("");
                e.setApellido1("");
                e.setApellido2("");
                e.setTipoId_idTipoId("");
                e.setIdentificacion("");
                e.setPerfilUsuario_idPerfilUsuario("");
                e.setEmail("");
                e.setTelefono("");
                e.setDireccion("");
                e.setCiudad("");
                e.setPais("");
                return res;
                // return new Gson().toJson(e);
                // return String.format(e.toString());   
                // return "(\"id\":\""+e.getIdperfilUsuario()+"\",\"nombre\":\""+e.getNomPerfilusuario()+"\")";
            } else {
                //return String.format("Usuario no encontrado.");
                //return "{\"value\":\""+"Usuario no encontrado."+"\"}";
                return new Gson().toJson(e);
            }
        }

        @GetMapping("/hola") //nombre de la pagina principal
        // primer metodo
        public String hola(@RequestParam(value = "name",defaultValue = "world")String name
                            ,@RequestParam(value = "edad",defaultValue = "20")String edad){
            return String.format("Hola %s, tu edad es %s anos!", name, edad);
        }
}
