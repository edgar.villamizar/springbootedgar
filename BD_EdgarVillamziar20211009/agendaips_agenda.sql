-- MySQL dump 10.13  Distrib 8.0.26, for Win64 (x86_64)
--
-- Host: localhost    Database: agendaips
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `agenda` (
  `citaDia` date NOT NULL,
  `citaHora` varchar(45) NOT NULL,
  `doctorUsuario` varchar(25) NOT NULL,
  `pacienteUsuario` varchar(25) DEFAULT NULL,
  `diagnostico` varchar(45) DEFAULT NULL,
  `medicamentos1` varchar(15) DEFAULT NULL,
  `medicamentos2` varchar(15) DEFAULT NULL,
  `medicamentos3` varchar(15) DEFAULT NULL,
  `idExam1` varchar(15) DEFAULT NULL,
  `idExam2` varchar(15) DEFAULT NULL,
  `idExam3` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`citaDia`,`citaHora`,`doctorUsuario`),
  KEY `fk_agenda_Usuarios1_idx` (`doctorUsuario`),
  KEY `fk_agenda_medicamentos1_idx` (`medicamentos1`),
  KEY `fk_agenda_examLabDiag1_idx` (`idExam1`),
  KEY `fk_agenda_examLabDiag2_idx` (`idExam2`),
  KEY `fk_agenda_examLabDiag3_idx` (`idExam3`),
  KEY `fk_agenda_medicamentos2_idx` (`medicamentos3`),
  KEY `fk_agenda_medicamentos3_idx` (`medicamentos2`),
  KEY `fk_agenda_Usuarios2_idx` (`pacienteUsuario`),
  CONSTRAINT `fk_agenda_examLabDiag1` FOREIGN KEY (`idExam1`) REFERENCES `examlabdiag` (`idExam`),
  CONSTRAINT `fk_agenda_examLabDiag2` FOREIGN KEY (`idExam2`) REFERENCES `examlabdiag` (`idExam`),
  CONSTRAINT `fk_agenda_examLabDiag3` FOREIGN KEY (`idExam3`) REFERENCES `examlabdiag` (`idExam`),
  CONSTRAINT `fk_agenda_medicamentos1` FOREIGN KEY (`medicamentos1`) REFERENCES `medicamentos` (`idMedicamento`),
  CONSTRAINT `fk_agenda_medicamentos2` FOREIGN KEY (`medicamentos3`) REFERENCES `medicamentos` (`idMedicamento`),
  CONSTRAINT `fk_agenda_medicamentos3` FOREIGN KEY (`medicamentos2`) REFERENCES `medicamentos` (`idMedicamento`),
  CONSTRAINT `fk_agenda_Usuarios1` FOREIGN KEY (`doctorUsuario`) REFERENCES `usuarios` (`Login`),
  CONSTRAINT `fk_agenda_Usuarios2` FOREIGN KEY (`pacienteUsuario`) REFERENCES `usuarios` (`Login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agenda`
--

LOCK TABLES `agenda` WRITE;
/*!40000 ALTER TABLE `agenda` DISABLE KEYS */;
/*!40000 ALTER TABLE `agenda` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-09 19:33:15
